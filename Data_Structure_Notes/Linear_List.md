# Linear_List
### 第2章 线性表

线性表的定义

线性表是具有相同数据类型的 n（n≥0）个数据元素的有限序列，其中 n 为表长，当 n = 0 时线性表是一个空表。若用L命名线性表，则其一般表示为 L = (a_1, a_2, … , a_i, a_i+1, … , a_n)

几个概念：

ai是线性表中的“第i个”元素线性表中的位序。

a_1是表头元素；a_n是表尾元素。

除第一个元素外，每个元素有且仅有一个直接前驱；除最后一个元素外，每个元素有且仅有一个直接后继。

线性表的基本操作

InitList(&L)：初始化表。构造一个空的线性表L，分配内存空间。

DestroyList(&L)：销毁操作。销毁线性表，并释放线性表L所占用的内存空间。

ListInsert(&L,i,e)：插入操作。在表L中的第i个位置上插入指定元素e。

ListDelete(&L,i,&e)：删除操作。删除表L中第i个位置的元素，并用e返回删除元素的值。

LocateElem(L,e)：按值查找操作。在表L中查找具有给定关键字值的元素。

GetElem(L,i)：按位查找操作。获取表L中第i个位置的元素的值。

其他常用操作：

Length(L)：求表长。返回线性表L的长度，即L中数据元素的个数。

PrintList(L)：输出操作。按前后顺序输出线性表L的所有元素值。

Empty(L)：判空操作。若L为空表，则返回true，否则返回false。

Tips：

对数据的操作（记忆思路） —— 创销、增删改查

C语言函数的定义 —— <返回值类型> 函数名 (<参数1类型> 参数1，<参数2类型> 参数2，……)

实际开发中，可根据实际需求定义其他的基本操作

函数名和参数的形式、命名都可改变（Reference：严蔚敏版《数据结构》）

什么时候要传入引用“&” —— 对参数的修改结果需要“带回来”

顺序表的定义

线性表是具有相同数据类型的 n（n≥0）个数据元素的有限序列

顺序表--用顺序存储的方式实现线性表顺序存储。把逻辑上相邻的元素存储在物理位置上也相邻的存储单元中，元素之间的关系由存储单元的邻接关系来体现。

```
typedef struct {
int num; //号数
int people; //人数
} Customer;
```

如何知道一个数据元素大小？

C语言 sizeof(ElemType)

Eg:

sizeof(int) = 4B

sizeof(Customer) = 8B

ElemType 就是你的顺序表中存放的数据元素类型

设线性表第一个元素的存放位置是 LOC (L)，LOC 是 location 的缩写





