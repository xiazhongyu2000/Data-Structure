//循环队列(circular_queue)
#include <stdio.h>
#include <stdlib.h>
#define MaxSize 50 //定义队列中元素的最大个数
typedef int ElemType;
typedef struct
{
    ElemType data[MaxSize]; //存放队列元素
    int front, rear;        //队头指针和队尾指针
} SqQueue;
//初始化
void InitQueue(SqQueue &Q)
{
    Q.rear = Q.front = 0; //初始化队首、队尾指针
}
//判队空
bool isEmpty(SqQueue Q)
{
    if (Q.rear == Q.front) //队空条件
        return true;
    else
        return false;
}
//入队
bool EnQueue(SqQueue &Q,ElemType x){
    if((Q.rear+1)%MaxSize==Q.front) //队满则报错
    return false;
    Q.data[Q.rear]=x;
    Q.rear=(Q.rear+1)%MaxSize; //队尾指针加1取模
    return true;
}
//出队
bool DeQueue(SqQueue &Q,ElemType &x){
    if(Q.rear==Q.front) //队空则报错
    return false;
    x=Q.data[Q.front];
    Q.front=(Q.front+1)%MaxSize; //队头指针加1取模
    return true;
}
int main()
{
    SqQueue Q;
    InitQueue(Q);
    
    EnQueue(Q,1);
    EnQueue(Q,2);
    EnQueue(Q,3);
    EnQueue(Q,4);
    EnQueue(Q,5);
    EnQueue(Q,6);
    EnQueue(Q,7);
    EnQueue(Q,8);
    EnQueue(Q,9);
    EnQueue(Q,10);

    return 0;
}