# Data-Structure
### 数据结构

[第1章 绪论](Data_Structure_Notes/Introduction.md)

[第2章 线性表](Data_Structure_Notes/Linear_List.md)

第3章 栈、队列和数组

第4章 串

第5章 树与二叉树

第6章 图

第7章 查找

第8章 排序

### 开发环境：[Visual Studio Code](https://code.visualstudio.com/),[Visual Studio 2022](https://visualstudio.microsoft.com/).
### 参考文献
[1]数据结构：C语言版/严蔚敏，吴伟民编著.-北京：清华大学出版社，1997.4(2020.11重印) ISBN 978-7-302-02368-5  
[2]2023年数据结构考研复习指导/王道论坛组编.-北京：电子工业出版社，2021.12(王道考研系列) ISBN 978-7-121-42436-6  
[3]大话数据结构：溢彩加强版/程杰著.-北京：清华大学出版社，2020.12(2021重印) ISBN 978-7-302-56471-3  
