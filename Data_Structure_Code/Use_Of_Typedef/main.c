#include<stdio.h>
//给结构体类型起别名，叫stu,起结构体指针类型的别名，叫pstu
typedef struct student {
	int num;
	char name[20];
	char sex;
}stu, * pstu;
typedef int INTEGER;//为什么要对int起别名，为了代码即注释
int main()
{
	stu s = { 1001,"wangle",'M' };
	pstu p;//stu* p1,那么p1也是一个结构体指针
	INTEGER i = 10;
	p = &s;
	printf("i=%d,p->num=%d\n", i, p->num);
	return 0;
}