#include<stdio.h>
#include<stdlib.h>
#define MAXLEN 255 //预定义最大串长为255
typedef struct { //定长顺序存储表示
	char ch[MAXLEN]; //每个分量存储一个字符
	int length; //串的实际长度
}SString;
StrAssign(&T, chars) //赋值操作。把串T赋值为chars。
StrCopy(&T, S) //复制操作。由串S复制得到串T。
bool StrEmpty(S) //判空操作。若S为空串，则返回TRUE，否则返回FALSE。
StrLength(S) //求串长。返回串S的元素个数。
ClearString(&S) //清空操作。将S清为空串。
DestroyString(&S) //销毁串。将串S销毁（回收存储空间）。
Concat(&T, S1, S2) //串联接。用T返回由S1和S2联接而成的新串
//用Sub返回串S的第pos个字符起长度为len的子串。
bool SubString(SString& Sub, SString S, int pos, int len) 
{
	//子串范围越界
	if (pos + len - 1 > S.length)
		return false;
	for (int i = pos; i < pos + len; i++)
		Sub.ch[i - pos + 1] = S.ch[i];
	Sub.length = len;
	return true;
}
//比较操作。若S>T,则返回值>0;若S=T,则返回=0;若S<T，则返回值<0。
int StrCompare(SString S, SString T) {
	for()
}
//朴素模式匹配算法
int Index(SString S, SString T) {
	int i = 1, j = 1;
	while (i <= S.length && j <= T.length) {
		if (S.ch[i] == T.ch[j]{
			++i; ++j; //继续比较后继字符
			}
		else {
			i=i-j+2;
				j=1; //指针后退重新开始匹配
		}
	}
	if (j > T.length)
		return i - T.length;
	else
		return 0;
}
//KMP算法
int Index_KMP(SString S, SSTRING T, int next[]) {
	int i = 1, j = 1;
	while (i <= S.length && j <= T.length) {
		if (j == 0 || S.ch[i] == T.ch[j]) {
			++i;
			++j; //继续比较后继字符
		}
		else
			j = next[j]; //模式串向右移动
	}
	if (j > T.length)
		return i - T.length; //匹配成功
	else
		return 0;
}
void get_next(String T, int next[]) {
	int i = 1, j = 0;
	next[1] = 0;
	while (i < T.length) {
		if (j == 0 || T.ch[i] == T.ch[j]) {
			++i; ++j;
			next[i] = j; //若p_i=p_j,则next[j+1]=next[j]+1
		}
		else
			j = next[j]; //否则令j=next[j],循环继续
	}
}
void get_nextval(String T, int nextval[]) {
	int i = 1, j = 0;
	nextval[1] = 0;
	while (i < T.length) {
		if (j == 0 || T.ch[i] == T.ch[j]) {
			++i; ++j;
			if (T.ch[i] != T.ch[j])
				nextval[i] = j;
			else
				nextval[i] = nextval[j];
		}
		else
			j = nextval[j];
	}
}
int main()
{
	SString S;

	return 0;
}