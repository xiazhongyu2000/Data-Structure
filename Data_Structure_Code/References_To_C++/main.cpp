#include<stdio.h>
#include<stdlib.h>
//把&写到形参的位置是C++的语法，称为引用，这个时候操作b和在主函数里边使用a等价
void modify_num(int& b)
{
	++b;
}
void modify_pointer(int*& p)//在子函数内操作p和主函数操作p手法一致
{
	p = (int*)malloc(20);
	p[0] = 5;
}
int main()
{
	int a = 10;
	modify_num(a);
	printf("a=%d\n", a);
	int* p = NULL;
	modify_pointer(p);
	printf("p[0]=%d\n", p[0]);
	return 0;
}